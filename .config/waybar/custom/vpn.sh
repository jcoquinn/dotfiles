#!/usr/bin/env bash

vpn=$(nmcli -t -f type,active,device,name con | grep '^vpn:y')

[ -z "$vpn" ] && exit

IFS_="$IFS"
IFS=':'
read -r _ _ device name <<< $vpn
IFS="$IFS_"

ipaddr=$(ip addr show vpn0 | grep 'inet ' | awk '{ print $2 }')

cat <<END | jq --compact-output --unbuffered
{
"text": "VPN",
"tooltip": "$device $name $ipaddr",
"class": "custom-vpn"
}
END
