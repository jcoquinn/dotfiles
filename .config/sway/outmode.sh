#!/bin/bash

cd ~/.config || exit

case $1 in
    -c)
        # Alacritty
        rm ~/.config/alacritty/override.yml
        touch ~/.config/alacritty/alacritty.yml

        # Waybar
        ln -sf ~/.config/waybar/none.css ~/.config/waybar/override.css

        # Sway
        rm ~/.config/sway/font
        swaymsg reload
        swaymsg 'output eDP-1 disable; output DP-4 enable'

        # Gammastep
        pkill gammastep && gammastep -orP
        ;;
    -l)
        # Alacritty
        ln -sf ~/.config/alacritty/fwrk.yml ~/.config/alacritty/override.yml
        touch alacritty/alacritty.yml

        # Waybar
        ln -sf ~/.config/waybar/fwrk.css ~/.config/waybar/override.css

        # Sway
        ln -sf ~/.config/sway/fwrk-font ~/.config/sway/font
        swaymsg reload
        swaymsg 'output eDP-1 enable; output DP-4 disable'

        # Gammastep
        pkill gammastep && gammastep -orP
        ;;
esac
