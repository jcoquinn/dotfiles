from libqtile import layout
from libqtile.config import Match

import hooks  # noqa: F401
from keys import keys  # noqa: F401
from groups import groups  # noqa: F401
from mouse import mouse  # noqa: F401
from screens import screens  # noqa: F401
from settings import theme


extension_defaults = theme.extension_defaults
layout_defaults = theme.layout_defaults
widget_defaults = theme.widget_defaults

layouts = [
    layout.Max(**layout_defaults),
    layout.Stack(num_stacks=2, **layout_defaults),
    layout.Columns(**layout_defaults),
]

floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ],
    **layout_defaults
)

auto_fullscreen = True
bring_front_click = False
cursor_warp = False
dgroups_key_binder = None
dgroups_app_rules = []
follow_mouse_focus = True
focus_on_window_activation = "smart"


# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
