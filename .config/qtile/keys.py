from libqtile.config import Key
from libqtile.lazy import lazy
from settings import alt, mod, terminal

keys = [
    # ---------------------------------------------------------------------
    # Qtile control
    # ---------------------------------------------------------------------
    Key(
        [mod, "control"],
        "r",
        lazy.restart(),
        desc="Restart Qtile",
    ),
    Key(
        [mod, "shift"],
        "e",
        lazy.shutdown(),
        desc="Shutdown Qitle",
    ),
    # ---------------------------------------------------------------------
    # Launch / Spawn / Run
    # ---------------------------------------------------------------------
    Key(
        [mod],
        "Return",
        lazy.spawn(terminal),
        desc="Spawn a terminal",
    ),
    Key(
        [mod],
        "r",
        lazy.spawncmd(),
        desc="Spawn command in bar",
    ),
    Key(
        [mod],
        "d",
        lazy.spawn("/usr/bin/rofi -show drun"),
        desc="Rofi drun",
    ),
    Key(
        [mod, "control"],
        "m",
        lazy.spawn("autorandr mobile"),
        desc="Change autorandr profile",
    ),
    Key(
        [mod, "control"],
        "d",
        lazy.spawn("autorandr docked"),
        desc="Change autorandr profile",
    ),
    Key(
        [alt, "control"],
        "f",
        lazy.spawn("/usr/bin/pcmanfm"),
        desc="Launch file manager",
    ),
    Key(
        [alt, "control"],
        "w",
        lazy.spawn("/usr/bin/firefox"),
        desc="Launch web browser",
    ),
    Key(
        [],
        "Print",
        lazy.spawn(
            (
                "shotgun -g $(hacksaw -c '#ff0000') "
                "$(xdg-user-dir PICTURES)/screenshot_$(date +%Y-%m-%d_%T).png"
            ),
            shell=True,
        ),
        desc="Screenshot",
    ),
    # ---------------------------------------------------------------------
    # Monitor switching
    # ---------------------------------------------------------------------
    Key(
        [mod],
        "n",
        lazy.prev_screen(),
        desc="Switch to prev monitor",
    ),
    Key(
        [mod],
        "m",
        lazy.next_screen(),
        desc="Switch to next monitor",
    ),
    # Key(
    # [mod],
    # "d",
    # lazy.to_screen(0),
    # desc="Warp to primary screen",
    # ),
    Key(
        [mod],
        "s",
        lazy.to_screen(1),
        desc="Warp to screen left of primary",
    ),
    Key(
        [mod],
        "f",
        lazy.to_screen(2),
        desc="Warp to screen right of primary",
    ),
    # ---------------------------------------------------------------------
    # Group switching
    # ---------------------------------------------------------------------
    Key(
        [mod],
        "comma",
        lazy.screen.prev_group(),
        desc="Switch to prev group",
    ),
    Key(
        [mod],
        "period",
        lazy.screen.next_group(),
        desc="Switch to next group",
    ),
    Key(
        [mod],
        "slash",
        lazy.screen.toggle_group(),
        desc="Switch to last used group",
    ),
    # ---------------------------------------------------------------------
    # Window focus
    # ---------------------------------------------------------------------
    Key(
        [mod],
        "space",
        lazy.layout.next(),
        desc="Focus next window",
    ),
    Key(
        [mod],
        "h",
        lazy.layout.left(),
        desc="Focus left window",
    ),
    Key(
        [mod],
        "l",
        lazy.layout.right(),
        desc="Focus right window",
    ),
    Key(
        [mod],
        "j",
        lazy.layout.down(),
        desc="Focus down window",
    ),
    Key(
        [mod],
        "k",
        lazy.layout.up(),
        desc="Focus up window",
    ),
    Key(
        [mod, "shift"],
        "q",
        lazy.window.kill(),
        desc="Close/kill/quit focused window",
    ),
    # ---------------------------------------------------------------------
    # Window movement
    # ---------------------------------------------------------------------
    Key(
        [mod, "shift"],
        "h",
        lazy.layout.client_to_next().when(layout="stack"),
        lazy.layout.shuffle_left().when(layout="columns"),
        desc="Move window left",
    ),
    Key(
        [mod, "shift"],
        "l",
        lazy.layout.client_to_next().when(layout="stack"),
        lazy.layout.shuffle_right().when(layout="columns"),
        desc="Move window right",
    ),
    Key(
        [mod, "shift"],
        "j",
        lazy.layout.shuffle_down().when(layout="columns"),
        desc="Move window down",
    ),
    Key(
        [mod, "shift"],
        "k",
        lazy.layout.shuffle_up().when(layout="columns"),
        desc="Move window up",
    ),
    # ---------------------------------------------------------------------
    # Window sizing
    # ---------------------------------------------------------------------
    Key(
        [mod, "control"],
        "h",
        lazy.layout.grow_left(),
        desc="Grow left window",
    ),
    Key(
        [mod, "control"],
        "l",
        lazy.layout.grow_right(),
        desc="Grow right window",
    ),
    Key(
        [mod, "control"],
        "j",
        lazy.layout.grow_down(),
        desc="Grow down window",
    ),
    Key(
        [mod, "control"],
        "k",
        lazy.layout.grow_up(),
        desc="Grow up window",
    ),
    Key(
        [mod, "control"],
        "n",
        lazy.layout.normalize(),
        desc="Reset window sizes",
    ),
    Key(
        [mod, "control"],
        "f",
        lazy.window.toggle_fullscreen(),
        desc="Toggle fullscreen",
    ),
    # ---------------------------------------------------------------------
    # Layout control
    # ---------------------------------------------------------------------
    Key(
        [mod],
        "Tab",
        lazy.next_layout(),
        desc="Switch layout",
    ),
    Key(
        [mod, "shift"],
        "f",
        lazy.window.toggle_floating(),
        desc="Toggle floating",
    ),
    # ---------------------------------------------------------------------
    # XF86 control
    # ---------------------------------------------------------------------
    Key(
        [],
        "XF86MonBrightnessUp",
        lazy.spawn("/usr/bin/xbacklight +10"),
        desc="Increase monitor brightness",
    ),
    Key(
        [],
        "XF86MonBrightnessDown",
        lazy.spawn("/usr/bin/xbacklight -10"),
        desc="Decrease monitor brightness",
    ),
    Key(
        [],
        "XF86AudioMute",
        lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ toggle"),
        desc="Toggle Mute",
    ),
    Key(
        [],
        "XF86AudioLowerVolume",
        lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ -5%"),
        desc="Lower sound volume",
    ),
    Key(
        [],
        "XF86AudioRaiseVolume",
        lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ +5%"),
        desc="Raise sound volume",
    ),
    # ---------------------------------------------------------------------
    # Lock, logout, suspend
    # ---------------------------------------------------------------------
    Key(
        [alt, "control"],
        "Delete",
        lazy.spawn("slock"),
        desc="Lock screen",
    ),
    Key(
        [alt, "control"],
        "Escape",
        lazy.spawn("lxsession-logout"),
        desc="Logout",
    ),
    Key(
        [alt, "control"],
        "grave",
        lazy.spawn("systemctl suspend"),
        desc="Suspend",
    ),
]
