class One:
    bar_size = 27
    bar_background = "#171717"
    fg_color = "#e5e1d8"

    def __init__(self):
        self._layout_defaults = dict(
            border_normal="#1f1f1f",
            border_focus="#448888",
            # border_focus="#7f7f7f",
            # border_focus="#719BC8",
            # border_focus="#2364b6",
            # border_focus="#4586d8",
            border_width=2,
        )
        self._widget_defaults = dict(
            font="JetBrains Mono NL",
            fontsize=14,
            foreground=self.fg_color,
            padding=6,
        )
        self._extension_defaults = self.widget_defaults.copy()

    @property
    def layout_defaults(self):
        return self._layout_defaults

    @property
    def widget_defaults(self):
        return self._widget_defaults

    @property
    def extension_defaults(self):
        return self._extension_defaults
