from keys import keys
from libqtile.config import DropDown, Group, Key, ScratchPad
from libqtile.lazy import lazy
from settings import alt, mod, terminal

groups = [
    Group(k, label=l)
    for k, l in [
        ("u", "WEB"),
        ("i", "DEVA"),
        ("o", "DEVB"),
        ("p", "TERM"),
        ("bracketleft", "DBMS"),
        ("bracketright", "COM"),
    ]
]

for g in groups:
    keys.extend(
        [
            Key(
                [mod],
                g.name,
                lazy.group[g.name].toscreen(toggle=False),
                desc="Switch group",
            ),
            Key(
                [mod, "shift"],
                g.name,
                lazy.window.togroup(g.name, switch_group=False),
                desc="Move focused window to group",
            ),
        ]
    )

groups.append(
    ScratchPad(
        "scratchpad",
        [
            DropDown(
                "term",
                terminal,
                height=0.7,
                opacity=0,
                border_normal="#000000",
                warp_pointer=False,
            )
        ],
    )
)

keys.append(
    Key(
        [alt, "control"],
        "t",
        lazy.group["scratchpad"].dropdown_toggle("term"),
        desc="Dropdown terminal",
    )
)
