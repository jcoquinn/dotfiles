import os
import subprocess

from libqtile import hook, qtile
from libqtile.log_utils import logger

wm_class_map = {
    "Pavucontrol": dict(w=700, h=600),
    "Pcmanfm": dict(w=800, h=700),
}


@hook.subscribe.client_new
def set_float(window):
    wm_inst, wm_class = window.window.get_wm_class()
    if wm_class in wm_class_map:
        window.floating = True
        window.fullscreen = False
        window.tweak_float(**wm_class_map[wm_class])


@hook.subscribe.startup_once
def startup_once():
    base = os.path.expanduser("~/.config/qtile")
    script = f"{base}/autostart.sh"
    if os.path.exists(script):
        with open(f"{base}/autostart.log", "w") as log:
            subprocess.run([script], stdout=log, stderr=log)
