from libqtile import bar, widget
from libqtile.config import Screen

from settings import theme


curr_screen_args = {
    "active_color": theme.fg_color,
    "active_text": "*",
    "inactive_text": " ",
}
group_box_args = {
    "highlight_method": "block",
    "inactive": "707070",
    "fontsize": 13,
}


screens = [
    Screen(
        top=bar.Bar(
            [
                widget.CurrentScreen(**curr_screen_args),
                widget.GroupBox(**group_box_args),
                widget.Prompt(),
                widget.WindowName(),
                widget.CurrentLayout(),
                widget.Systray(),
                widget.Clock(format="[%m/%d %a %H:%M]"),
            ],
            size=theme.bar_size,
            background=theme.bar_background,
        ),
    ),
    Screen(
        top=bar.Bar(
            [
                widget.CurrentScreen(**curr_screen_args),
                widget.GroupBox(**group_box_args),
                widget.Spacer(),
                widget.CurrentLayout(),
            ],
            size=theme.bar_size,
            background=theme.bar_background,
        ),
    ),
    Screen(
        top=bar.Bar(
            [
                widget.CurrentScreen(**curr_screen_args),
                widget.GroupBox(**group_box_args),
                widget.Spacer(),
                widget.CurrentLayout(),
            ],
            size=theme.bar_size,
            background=theme.bar_background,
        ),
    ),
]
