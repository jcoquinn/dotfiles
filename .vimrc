so ~/.vim/defaults.vim

let mapleader = ' '
so ~/.vim/plugins.vim

" ------------
" Key Mappings
" ------------
nnoremap q <Esc>
inoremap jk <Esc>
vnoremap <Leader>jk <Esc>

nnoremap H ^
nnoremap L $

nnoremap M :bnext<CR>
nnoremap N :bprev<CR>
nnoremap <Leader>b :buffers<CR>:b

nnoremap <Leader>h <C-w>h
nnoremap <Leader>j <C-w>j
nnoremap <Leader>k <C-w>k
nnoremap <Leader>l <C-w>l

" inoremap (<CR> (<CR>)<ESC>O<TAB>
" inoremap [<CR> [<CR>]<ESC>O<TAB>
" inoremap {<CR> {<CR>}<ESC>O

" --------
" Commands
" --------
com! Aleon :ALEEnable | let g:ale_fix_on_save = 1
com! Aleoff :ALEDisable | let g:ale_fix_on_save = 0

" ------
" Colors
" ------
" highlight
hi Error ctermbg=DarkRed ctermfg=White
hi MatchParen ctermbg=DarkGray ctermfg=Black
hi SpellBad ctermbg=DarkRed ctermfg=Black
hi SpellCap ctermbg=LightBlue ctermfg=Black

" -------
" Options
" -------
" autoindent
set ai

" backspace
set bs=indent,eol,start

" cursorline: highlight current line
set cul

" encoding: character encoding
set enc=utf-8

" exrc: search current directory for vim files
set exrc

" esckeys: ignore insert mode Fn key seq
set noek

" expandtab: expand tabs to spaces
set et

" foldenable
set nofen

" foldmethod
set fdm=indent

" hidden: don't require saving buffer
set hid

" hlsearch: highlight search matches
set hls

" ignorecase: when searching
set ic

" laststatus
set ls=2

" lazyredraw: redraw when necessary
set lz

" linebreak
" set lbr

" list: show $ at EOL
set nolist

" number: show line nums
set nu

" ruler
set noruler

" secure: don't allow shell or write commands
set secure

" showcmd
set nosc

" showmatch: highlight matching [{()}]
" set sm

" showmode: show mode on last line
set nosmd

" showtabline
set showtabline=2

" smartindent
set si

" softtabstop: # cols of whitespace for tab keypress
set sts=4

" shiftwidth: # of cols to indent using >> | <<
set sw=4

" tabstop: # cols of whitespace for \t
set ts=4

" termguicolors
" if has('termguicolors')
    " set tgc
" endif

" wrap: wrap lines
set nowrap

" wildmenu
" set nowmnu

" wildmode
" set wim=list:longest

" ------------
" Autocommands
" ------------
" augroup OpenFolds
    " autocmd!
    " autocmd BufRead * normal zR
" augroup END
