#!/bin/bash

[ -f "/etc/profile" ] && . /etc/profile

export EDITOR='/usr/bin/nvim'
export HISTCONTROL='ignoreboth'
export HISTFILESIZE=5000
export HISTIGNORE='l:la'
export HISTSIZE=5000

export BUN_INSTALL="$HOME/.bun"
export FZF_DEFAULT_COMMAND='fd -HL -E .git'
export NVM_DIR="$HOME/.nvm"
export PNPM_HOME="$HOME/.local/share/pnpm"
export PYENV_ROOT="$HOME/.pyenv"
export SDKMAN_DIR="$HOME/.sdkman"

bin_paths=(
    "$HOME/.local/bin"
    "$HOME/bin"
    "$HOME/go/bin"
    "$HOME/.cargo/bin"
    "$HOME/.deno/bin"
    "$HOME/.config/composer/vendor/bin"
    "$BUN_INSTALL/bin"
    "$PNPM_HOME"
    "$PYENV_ROOT/bin"
)

IFS_=$IFS
IFS=':'
export PATH="$PATH:${bin_paths[*]}"
IFS=$IFS_

[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"

if [ -x "$PYENV_ROOT/bin/pyenv" ]; then
    eval "$(pyenv init -)"
    pyenv global 3.12
fi

if [ -z "$DISPLAY" ] && [ "$(tty)" = '/dev/tty1' ]; then
    case "$(cat /etc/hostname)" in
        l5400) startx ;;
        *) . ~/.winitrc ;;
    esac
fi
