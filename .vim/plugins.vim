call plug#begin('~/.vim/plugged')
Plug 'dense-analysis/ale'
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/asyncomplete-lsp.vim'
Plug 'chriskempson/base16-vim'
" Plug 'mattn/emmet-vim'
Plug 'junegunn/fzf.vim'
Plug 'lambdalisue/fern.vim'
Plug 'itchyny/lightline.vim'
Plug 'preservim/nerdcommenter'
" Plug 'qpkorr/vim-bufkill'
Plug 'ap/vim-buftabline'
" Plug 'bfrg/vim-cpp-modern'
" Plug 'fatih/vim-go'
Plug 'vim-python/python-syntax'
Plug 'prabirshrestha/vim-lsp'
Plug 'rhysd/vim-lsp-ale'
Plug 'mattn/vim-lsp-settings'
" Plug 'sheerun/vim-polyglot'
Plug 'mhinz/vim-signify'
Plug 'chaoren/vim-wordmotion'
call plug#end()

" ---
" ALE
" ---
let g:ale_disable_lsp = 1
let g:ale_fix_on_save = 1
let g:ale_fixers = {'*': ['remove_trailing_lines', 'trim_whitespace']}
let g:ale_lint_delay  = 1000
let g:ale_linters_explicit = 1
let g:ale_use_global_executables = 1

" ------------
" asyncomplete
" ------------
let g:asyncomplete_auto_popup = 0
" let g:asyncomplete_popup_delay = 1000
imap <C-@> <Plug>(asyncomplete_force_refresh)

" ----------
" base16-vim
" ----------
let base16colorspace = 256
" colo base16-seti
colo base16-tomorrow-night

" ----------
" Buftabline
" ----------
let g:buftabline_indicators = 1
let g:buftabline_numbers = 1
" see :so $VIMRUNTIME/syntax/hitest.vim
hi link BufTabLineCurrent LightlineLeft_normal_0_1
hi link BufTabLineActive LightlineRight_normal_0_1
hi link BufTabLineHidden Tabline
hi link BufTabLineModifiedCurrent LightlineLeft_normal_0_1
hi link BufTabLineModifiedActive LightlineRight_normal_0_1
hi link BufTabLineModifiedHidden Tabline

" ----
" Fern
" ----
let g:fern#default_exclude = '^\.git$\|\.venv\|.*cache\|__pycache__$'
let g:fern#default_hidden = 1
let g:fern#disable_default_mappings = 1
nnoremap <silent> <Leader>fe :Fern . -drawer -reveal=% -toggle -width=32<CR>

function! s:fern_init() abort
    nmap <buffer> c <Plug>(fern-action-copy)
    nmap <buffer> d <Plug>(fern-action-remove)
    nmap <buffer> e <Plug>(fern-action-open:select)
    nmap <buffer> h <Plug>(fern-action-collapse)
    nmap <buffer> H <Plug>(fern-action-hidden:toggle)
    nmap <buffer> l <Plug>(fern-action-open-or-expand)
    nmap <buffer> m <Plug>(fern-action-move)
    nmap <buffer> M <Plug>(fern-action-rename)
    nmap <buffer> n <Plug>(fern-action-new-path)
    nmap <buffer> p <Plug>(fern-action-focus:parent)
    nmap <buffer> r <Plug>(fern-action-reload)
    nmap <buffer> * <Plug>(fern-action-mark:toggle)
    nmap <buffer> <nowait> < <Plug>(fern-action-leave)
    nmap <buffer> <nowait> > <Plug>(fern-action-enter)
endfunction

augroup fern_group
    autocmd! *
    autocmd FileType fern call s:fern_init()
augroup END

" -------
" fzf.vim
" -------
nnoremap <C-f> :BLines<CR>
nnoremap <C-o> :Files<CR>
nnoremap <Tab> :Buffers<CR>
nnoremap <C-s> :Lines<CR>

" --------------------
" lightline-bufferline
" --------------------
" let g:lightline#bufferline#show_number = 1

" ---------
" lightline
" ---------
" let g:lightline = {}
" let g:lightline.tabline = {'left': [['buffers']], 'right': [['close']]}
" let g:lightline.component_expand = {'buffers': 'lightline#bufferline#buffers'}
" let g:lightline.component_type = {'buffers': 'tabsel'}

" -------------
" NERDCommenter
" -------------
let g:NERDAltDelims_c = 1
let g:NERDAltDelims_python = 1
let g:NERDCommentEmptyLines = 1
let g:NERDDefaultAlign = 'left'
let g:NERDSpaceDelims = 1

" --------
" netrw
" --------
" disable netrw
let g:loaded_netrw  = 1
let g:loaded_netrwPlugin = 1

" -------------
" python-syntax
" -------------
let g:python_highlight_builtins = 1
let g:python_highlight_class_vars = 1
let g:python_highlight_exceptions = 1
let g:python_highlight_func_calls = 1
let g:python_highlight_string_format = 1
let g:python_highlight_string_formatting = 1

" -------
" Signify
" -------
set signcolumn=auto
set updatetime=1000
highlight clear SignColumn

" --------------
" vim-cpp-modern
" --------------
let g:cpp_member_highlight = 1

" -------
" vim-go
" -------
let g:go_fmt_command = "gopls"
let g:go_gopls_gofumpt = 1
let g:go_list_type = "quickfix"

let g:go_metalinter_autosave = 1
let g:go_metalinter_autosave_enabled = []
let g:go_metalinter_command = 'staticcheck'
let g:go_metalinter_enabled = []

let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_function_calls = 1
let g:go_highlight_types = 1

autocmd FileType go nnoremap <leader>b  <Plug>(go-build)
autocmd FileType go nnoremap <leader>d  <Plug>(go-doc)
autocmd FileType go nnoremap <leader>i  <Plug>(go-info)
autocmd FileType go nnoremap <leader>r  <Plug>(go-run)
autocmd FileType go nnoremap <leader>t  <Plug>(go-test)

nnoremap <C-n> :cnext<CR>
nnoremap <C-m> :cprevious<CR>
nnoremap <C-c> :cclose<CR>

" -------
" vim-lsp
" -------
let g:lsp_document_code_action_signs_enabled = 0
let g:lsp_signature_help_enabled = 0

nnoremap <buffer> gd <Plug>(lsp-definition)
nnoremap <buffer> gh <Plug>(lsp-hover)

" close preview window with <Esc>
autocmd User lsp_float_opened nmap <buffer> <silent> <Esc> <Plug>(lsp-preview-close)
autocmd User lsp_float_closed nunmap <buffer> <Esc>

" ----------------
" vim-lsp-settings
" ----------------
let g:lsp_settings_enable_suggestions = 0
let g:lsp_settings = {
    \ 'haskell-language-server': {
        \ 'disaled': v:true,
        \ 'cmd': ['haskell-language-server-wrapper', '--lsp']
    \ },
    \ 'pylsp': {
        \ 'disabled': v:false,
        \ 'workspace_config': {
            \ 'pylsp': {
                \ 'configurationSources': ['flake8'],
                \ 'plugins': {
                    \ 'flake8': {'enabled': v:true},
                    \ 'mccabe': {'enabled': v:false},
                    \ 'pycodestyle': {'enabled': v:false},
                    \ 'pyflakes': {'enabled': v:false},
                    \ 'rope_completion': {'enabled': v:false},
                    \ 'yapf': {'enabled': v:false},
                \ },
            \ },
        \ },
    \ },
    \ 'gopls': {
        \ 'disabled': v:true,
    \ },
\ }
