setl wrap textwidth=79
setl spell spelllang=en_us
let b:ale_fixers = ['prettier']
