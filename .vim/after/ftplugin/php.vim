setl ts=4 sts=4 sw=4
let b:ale_fix_on_save = 1
let b:ale_fixers = ['phpcbf']
let b:ale_linters = ['php', 'phpcs']
let b:ale_php_phpcbf_standard = 'PSR12'
let b:ale_php_phpcbf_use_global = 1
" let b:ale_php_phpcs_standard = '~/projects/work/svn/.phpcs.xml'
let b:ale_php_phpcs_standard = 'PSR12'
let b:ale_php_phpcs_use_global = 1
" let b:ale_fixers = ['php_cs_fixer']
" let b:ale_php_cs_fixer_use_global = 0
