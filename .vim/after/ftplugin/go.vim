setl ts=8 sts=8 sw=8 noet
let b:ale_fixers = ['gofumpt']
let b:ale_linters = ['staticcheck']
