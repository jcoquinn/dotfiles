let b:ale_c_build_dir = 'build'
let b:ale_c_parse_compile_commands = 1
let b:ale_fixers = ['clang-format']
